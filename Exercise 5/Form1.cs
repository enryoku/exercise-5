﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void CalculateButton_Click(object sender, EventArgs e)
        {
            // create integer variable to capture number of terms entered by user
            int numberOfCalculations = 0;
            // check to see if we can convert text box input into an integer
            if (int.TryParse(inputTextBox.Text, out numberOfCalculations))
            {
                // start by calculating first term of pi according to provided instructions
                double calculatedValueOfPi = 4.0 - 4.0 / 3.0;
                // set denominator to starting value indicated in above calculation
                double denominator = 3.0;

                // start for loop to calculate up to the number of terms provided by user
                for (int plusCounter = 0; plusCounter <= numberOfCalculations; plusCounter++)
                {
                    // check to see if we have already reached the number of terms to calculate
                    if (plusCounter <= numberOfCalculations) {
                        // if not add 2 to the denominator per the provided instructions
                        denominator += 2.0;
                        // calculate next term
                        calculatedValueOfPi += 4.0 / denominator;
                        // increment the counter variable
                        plusCounter++;
                        // check to see if we have already reached the number of terms to calculate
                        if (plusCounter <= numberOfCalculations)
                        {
                            // if not add 2 to the denominator per the provided instructions
                            denominator += 2.0;
                            // calculate the next term
                            calculatedValueOfPi -= 4.0 / denominator;
                            // increment the counter variable
                            plusCounter++;
                        }
                    }
                }
                // set the calculated value for PI to the label to display the answer
                valueOfPiLabel.Text = "= " + calculatedValueOfPi.ToString();
                // update the label containing a placeholder for the number of terms calculated
                iterationsLabel.Text = numberOfCalculations.ToString();
            } else
            {
                // if we can't convert text box input to an integer alert the user that they have input data the program can't use
                MessageBox.Show("Please enter a valid number of terms!! I was not able to convert " + inputTextBox.Text + " to an integer.");
            }
        }
    }
}
