﻿namespace Exercise_5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.numberOfTermsLabel = new System.Windows.Forms.Label();
            this.approximateValueLabel = new System.Windows.Forms.Label();
            this.inputTextBox = new System.Windows.Forms.TextBox();
            this.calculateButton = new System.Windows.Forms.Button();
            this.iterationsLabel = new System.Windows.Forms.Label();
            this.termsLabel = new System.Windows.Forms.Label();
            this.valueOfPiLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // numberOfTermsLabel
            // 
            this.numberOfTermsLabel.AutoSize = true;
            this.numberOfTermsLabel.Location = new System.Drawing.Point(15, 29);
            this.numberOfTermsLabel.Name = "numberOfTermsLabel";
            this.numberOfTermsLabel.Size = new System.Drawing.Size(85, 13);
            this.numberOfTermsLabel.TabIndex = 0;
            this.numberOfTermsLabel.Text = "Enter # of terms:";
            // 
            // approximateValueLabel
            // 
            this.approximateValueLabel.AutoSize = true;
            this.approximateValueLabel.Location = new System.Drawing.Point(15, 161);
            this.approximateValueLabel.Name = "approximateValueLabel";
            this.approximateValueLabel.Size = new System.Drawing.Size(141, 13);
            this.approximateValueLabel.TabIndex = 1;
            this.approximateValueLabel.Text = "Approximate value of pi after";
            // 
            // inputTextBox
            // 
            this.inputTextBox.Location = new System.Drawing.Point(124, 29);
            this.inputTextBox.Name = "inputTextBox";
            this.inputTextBox.Size = new System.Drawing.Size(115, 20);
            this.inputTextBox.TabIndex = 2;
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(18, 77);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(90, 46);
            this.calculateButton.TabIndex = 3;
            this.calculateButton.Text = "CALCULATE";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.CalculateButton_Click);
            // 
            // iterationsLabel
            // 
            this.iterationsLabel.AutoSize = true;
            this.iterationsLabel.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iterationsLabel.Location = new System.Drawing.Point(166, 161);
            this.iterationsLabel.Name = "iterationsLabel";
            this.iterationsLabel.Size = new System.Drawing.Size(0, 16);
            this.iterationsLabel.TabIndex = 4;
            // 
            // termsLabel
            // 
            this.termsLabel.AutoSize = true;
            this.termsLabel.Location = new System.Drawing.Point(217, 161);
            this.termsLabel.Name = "termsLabel";
            this.termsLabel.Size = new System.Drawing.Size(32, 13);
            this.termsLabel.TabIndex = 5;
            this.termsLabel.Text = "terms";
            // 
            // valueOfPiLabel
            // 
            this.valueOfPiLabel.AutoSize = true;
            this.valueOfPiLabel.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueOfPiLabel.Location = new System.Drawing.Point(15, 199);
            this.valueOfPiLabel.Name = "valueOfPiLabel";
            this.valueOfPiLabel.Size = new System.Drawing.Size(0, 16);
            this.valueOfPiLabel.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 244);
            this.Controls.Add(this.valueOfPiLabel);
            this.Controls.Add(this.termsLabel);
            this.Controls.Add(this.iterationsLabel);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.inputTextBox);
            this.Controls.Add(this.approximateValueLabel);
            this.Controls.Add(this.numberOfTermsLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Approximate PI";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label numberOfTermsLabel;
        private System.Windows.Forms.Label approximateValueLabel;
        private System.Windows.Forms.TextBox inputTextBox;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label iterationsLabel;
        private System.Windows.Forms.Label termsLabel;
        private System.Windows.Forms.Label valueOfPiLabel;
    }
}

